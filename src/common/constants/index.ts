export const SEQUELIZE = 'SEQUELIZE';
export const DATABASE = 'database';
export const ROLES = 'roles';
export const USER_REPOSITORY = 'USER_REPOSITORY';
export const TASK_REPOSITORY = 'TASK_REPOSITORY';
export const ADMIN_ROLE = 'admin';
export const USER_ROLE = 'user';
